#include "slave_side_telemanipulation-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>


//	RTT::InputPort<WRUT::TelemanipulationData> InputWavePortToRobot; //data containig 3 wave variables
//	RTT::InputPort<geometry_msgs::WrenchStamped> FromRobotForcePort; //data containing 3 forces
//
//	RTT::OutputPort<remedi_usg_ros_udp::Teleop_Feedback> OutputWavePortFromRobot; 			//data containing 3 wave variables
//	RTT::InputPort<geometry_msgs::TwistStamped> RobotCmdVelocityPort; // velocity form delta
//
//	RTT::OutputPort<std_msgs::UInt32> Telemanipulation_Status_out;  //output to DDM
//
//	RTT::OutputPort<geometry_msgs::TwistStamped> DecodedVelocityPort; // velocity form delta
//	RTT::OutputPort<geometry_msgs::WrenchStamped> DecodedForcePort; //data containing 3 forces

Slave_side_telemanipulation::Slave_side_telemanipulation(std::string const& name) : TaskContext(name){

	this->ports()->addPort( "InputWavePortToRobot", InputWavePortToRobot )
													.doc("Output port for InputWavePortToRobot" );

	this->ports()->addPort( "FromRobotForcePort", FromRobotForcePort )
													.doc("Output port for FromRobotForcePort" );

	this->ports()->addPort( "OutputWavePortFromRobot", OutputWavePortFromRobot )
													.doc("Output port for OutputWavePortFromRobot" );

	this->ports()->addPort( "RobotCmdVelocityPort", RobotCmdVelocityPort )
													.doc("Output port for RobotCmdVelocityPort" );

	this->ports()->addPort( "Telemanipulation_Status_out", Telemanipulation_Status_out )
													.doc("Input port for Telemanipulation_Status_out" );

//	this->ports()->addPort( "DecodedForcePort", DecodedForcePort )
//													.doc("Output port for DecodedForcePort" );

	this->ports()->addPort( "DecodedVelocityPort", DecodedVelocityPort )
													.doc("Output port for DecodedVelocityPort" );

	this->addOperation( "setb", &Slave_side_telemanipulation::setb,
			this, RTT::OwnThread).doc("reset");

	this->addOperation( "getb", &Slave_side_telemanipulation::getb,
			this, RTT::OwnThread).doc("reget");

	b=10;
	InputWaveDataAvailable = false;
	RobotForceDataAvailable = false;


	//	this->ports()->addEventPort( "State", State )
	//		    								.doc("Input port for platform state" );
	std::cout << "Slave_side_telemanipulation constructed !" <<std::endl;

}

bool Slave_side_telemanipulation::configureHook(){
	std::cout << "Slave_side_telemanipulation configured !" <<std::endl;
	return true;
}

bool Slave_side_telemanipulation::startHook(){
	std::cout << "Slave_side_telemanipulation started !" <<std::endl;
	actual_state = 0;
	initialize();

	setInputWaveDataAvailable();
	setRobotForceDataAvailable();
	  
	
	return true;
}

void Slave_side_telemanipulation::updateHook(){
	//
	//	1. Read from the InputWave port
	//	2. Read from haptic intreface port
	//	3. Compute values
	//	4. Write and send via OutputWave port

  //	std::cout << "Slave_side_telemanipulation updated !" <<std::endl;

	//	RTT::InputPort<WRUT::TelemanipulationData> InputWavePortToRobot; //data containig 3 wave variables
	//	RTT::InputPort<geometry_msgs::WrenchStamped> FromRobotForcePort; //data containing 3 forces
	//
	//	RTT::OutputPort<remedi_usg_ros_udp::Teleop_Feedback> OutputWavePortFromRobot; 			//data containing 3 wave variables
	//	RTT::InputPort<geometry_msgs::TwistStamped> RobotCmdVelocityPort; // velocity form delta
	//
	//	RTT::OutputPort<std_msgs::UInt32> Telemanipulation_Status_out;  //output to DDM
	//
	//	RTT::OutputPort<geometry_msgs::TwistStamped> DecodedVelocityPort; // velocity form delta
	//	RTT::OutputPort<geometry_msgs::WrenchStamped> DecodedForcePort; //data containing 3 forces

	remedi_usg_ros_udp::Teleop_Feedback OutputWaveDataFromRobot;
	geometry_msgs::PoseStamped DeltaPositionData;
	remedi_usg_ros_udp::ArmFeedback FromRobotForceData;
	std_msgs::UInt8 DDMStatusData;

	WRUT::TelemanipulationData InputWaveDataToRobot;
	geometry_msgs::WrenchStamped toDeltaForceData;
	std_msgs::UInt8 TelemanipulationStatusData;


	actual_state=DDM_CODE_STANDBY;
	//std::cout << "\033[2J\033[1;1H";

	//std::cout << "Przed \n";

	

	if(InputWavePortToRobot.read(InputWaveDataToRobot) == RTT::NewData){
	  		setInputWaveDataAvailable();
		std::cout << "Input Wave available \n";
//
//		InputWaveDataToRobot.waveVariable[0]=0;
//		InputWaveDataToRobot.waveVariable[1]=0;
//		InputWaveDataToRobot.waveVariable[2]=0;
	}
	//std::cout << "Po \n";
	
	if(FromRobotForcePort.read(FromRobotForceData) == RTT::NewData){
	    setRobotForceDataAvailable();
	    //	  FromRobotForceData.Wrench.wrench.force.x=0;
	    //FromRobotForceData.Wrench.wrench.force.y=0;
	    //FromRobotForceData.Wrench.wrench.force.z=0;
	  std::cout << "Force from robot unavailable \n";
	}

	//	if ( true){
	

	//	if(FromRobotForcePort.read(FromRobotForceData) == RTT::NewData){
	//  setRobotForceDataAvailable();
	// std::cout << "Force from robot available \n";
	//}

	if ( isRobotForceDataAvailable() && isInputWaveDataAvailable()){

	 
		std::cout << "************************************** " <<  std::endl;
		//actual_state = DDM_CODE_READY;
		//DeltaPositionPort.read(DeltaPositionData);

		// computing wave variable form
		assignmentOfInputsWave(InputWaveDataToRobot, FromRobotForceData);
		step();
		printOutData();

		// we have to send delta position and orientation, force to delta and the wave to robot
		setOutputValuesAndSend(DeltaPositionData, FromRobotForceData);

		clearInputWaveDataAvailable();
		clearRobotForceDataAvailable();

		setInputWaveDataAvailable();
		//clearInputWaveDataAvailable();
		
		std::cout << "************************************** " <<  std::endl;
	}

	//	std::cout << "Slave_side_telemanipulation executes updateHook !" <<std::endl;
}


void Slave_side_telemanipulation::printOutData(void ){

  std::cout << "Inputs to the blocks \n " << satler_filteredSlave_U.Fs_cmdX << std::endl;
  std::cout << "Force coming from slave x: " << satler_filteredSlave_U.Fs_cmdX << std::endl;
  std::cout << "Force coming from slave y: " << satler_filteredSlave_U.Fs_cmdY << std::endl;
  std::cout << "Force coming from slave z: " << satler_filteredSlave_U.Fs_cmdZ << std::endl;

  std::cout << "Wave Coming from delta x: " << satler_filteredSlave_U.Wu_rcvX << std::endl;
  std::cout << "Wave Coming from delta y: " << satler_filteredSlave_U.Wu_rcvY << std::endl;
  std::cout << "Wave Coming from delta z: " << satler_filteredSlave_U.Wu_rcvZ << std::endl;

  std::cout << "Wave going to delta x: " << satler_filteredSlave_Y.Wv_cmdX << std::endl;
  std::cout << "Wave going to delta y: " << satler_filteredSlave_Y.Wv_cmdY << std::endl;
  std::cout << "Wave going to delta z: " << satler_filteredSlave_Y.Wv_cmdZ << std::endl;

  std::cout << "Speed from master x: " << satler_filteredSlave_Y.Vs_rcvX << std::endl;
  std::cout << "Speed from master y: " << satler_filteredSlave_Y.Vs_rcvY << std::endl;
  std::cout << "Speed from master z: " << satler_filteredSlave_Y.Vs_rcvZ << std::endl;



}
		//setOutputValuesAndSend(DeltaPositionData,FromRobotForceData);
void Slave_side_telemanipulation::setOutputValuesAndSend(
		geometry_msgs::PoseStamped positionFromDelta,
		remedi_usg_ros_udp::ArmFeedback DataFromRobot){

	// we have to send delta position and orientation, force to delta and the wave to robot
	//setOutputValuesAndSend(DeltaPositionData,toDeltaForceData);

	remedi_usg_ros_udp::Teleop_Feedback waveDataToBeSendAndForce;
	geometry_msgs::TwistStamped velocityDataToBeSend;

	geometry_msgs::TwistStamped decodedVelocityToBeSend;
	geometry_msgs::WrenchStamped decodedForceToBeSend;

	waveDataToBeSendAndForce.Wave_var_OUT[0] = satler_filteredSlave_Y.Wv_cmdX;
	waveDataToBeSendAndForce.Wave_var_OUT[1] = satler_filteredSlave_Y.Wv_cmdY;
	waveDataToBeSendAndForce.Wave_var_OUT[2] = satler_filteredSlave_Y.Wv_cmdZ;

	waveDataToBeSendAndForce.Arm_Pose.pose.position.x = 0;
	waveDataToBeSendAndForce.Arm_Pose.pose.position.y = 0;
	waveDataToBeSendAndForce.Arm_Pose.pose.position.z = 0;

	waveDataToBeSendAndForce.Wrench.wrench.force.x = DataFromRobot.Wrench.wrench.force.x;
	waveDataToBeSendAndForce.Wrench.wrench.force.y = DataFromRobot.Wrench.wrench.force.y;
	waveDataToBeSendAndForce.Wrench.wrench.force.z = DataFromRobot.Wrench.wrench.force.z;


	OutputWavePortFromRobot.write(waveDataToBeSendAndForce);


//	  std::cout << "ForceFromRobot.wrench.force.x: " << ForceFromRobot.wrench.force.x << std::endl;
//	  std::cout << "ForceFromRobot.wrench.force.y: " << ForceFromRobot.wrench.force.y << std::endl;
//	  std::cout << "ForceFromRobot.wrench.force.z: " << ForceFromRobot.wrench.force.z << std::endl;


	//forceDataToBeSend.wrench.force.x = ForceFromRobot.wrench.force.x;
	//forceDataToBeSend.wrench.force.y = ForceFromRobot.wrench.force.y;
    //forceDataToBeSend.wrench.force.z = ForceFromRobot.wrench.force.z;


	velocityDataToBeSend.header.seq++;
	velocityDataToBeSend.header.stamp = rtt_rosclock::host_now();
	velocityDataToBeSend.twist.linear.x = satler_filteredSlave_Y.Vs_rcvX;
	velocityDataToBeSend.twist.linear.y = satler_filteredSlave_Y.Vs_rcvY;
	velocityDataToBeSend.twist.linear.z = satler_filteredSlave_Y.Vs_rcvZ;

	RobotCmdVelocityPort.write(velocityDataToBeSend);

	std::cout << "Success " << decodedForceToBeSend.wrench.force.x <<"\n";
	


}

void Slave_side_telemanipulation::assignmentOfInputsWave(
		WRUT::TelemanipulationData Wave,
		remedi_usg_ros_udp::ArmFeedback Data){


  	if (std::abs(Data.Wrench.wrench.force.x) < 0.4)
  		Data.Wrench.wrench.force.x=0;

  	if (std::abs(Data.Wrench.wrench.force.y) < 0.4)
 		Data.Wrench.wrench.force.y=0;

  	if (std::abs(Data.Wrench.wrench.force.z) < 0.4)
  		Data.Wrench.wrench.force.z=0;


  
  satler_filteredSlave_U.Fs_cmdX = Data.Wrench.wrench.force.x;
  satler_filteredSlave_U.Fs_cmdY = Data.Wrench.wrench.force.y;
  satler_filteredSlave_U.Fs_cmdZ = Data.Wrench.wrench.force.z;

  // satler_filteredSlave_U.Vm_cmdX = 0;
  // satler_filteredSlave_U.Vm_cmdY = 0;
  // satler_filteredSlave_U.Vm_cmdZ = 0;


	/*
	satler_filteredSlave_U.Vm_cmdX = Velocities.twist.angular.x;
	satler_filteredSlave_U.Vm_cmdY = Velocities.twist.angular.y;
	satler_filteredSlave_U.Vm_cmdZ = Velocities.twist.angular.z;
	*/

	satler_filteredSlave_U.Wu_rcvX = Wave.waveVariable[0];
	satler_filteredSlave_U.Wu_rcvY = Wave.waveVariable[1];
	satler_filteredSlave_U.Wu_rcvZ = Wave.waveVariable[2];

	std::cout << " Wave.Wave_var_OUT[0]: " << Wave.waveVariable[0] << std::endl;
	std::cout << " Wave.Wave_var_OUT[1]: " << Wave.waveVariable[1] << std::endl;
	std::cout << " Wave.Wave_var_OUT[2]: " << Wave.waveVariable[2] << std::endl;


}

void Slave_side_telemanipulation::stopHook() {
	std::cout << "Slave_side_telemanipulation executes stopping !" <<std::endl;
	actual_state=DDM_CODE_SHUTDOWN;
}

void Slave_side_telemanipulation::cleanupHook() {
	std::cout << "Slave_side_telemanipulation cleaning up !" <<std::endl;
}

//void Slave_side_telemanipulation::waveCoding(void ){
//	Fs_rcv = b * Vm_cmd - sqrt(2*b) * Wv_rcv;
//	Wu_cmd = -Wv_rcv + sqrt(2*b) * Vm_cmd;
//}

void Slave_side_telemanipulation::setb(float noweb, int no){

	switch(no)
	{
	case 0:
		satler_filteredSlave_P.bX = noweb;
		break;

	case 1:
		satler_filteredSlave_P.bY = noweb;
		break;

		//...
	case 2:
		satler_filteredSlave_P.bZ = noweb;
		break;
	}

	getb(no);
}

void Slave_side_telemanipulation::getb(int no){
	std::cout << "Value of b ";
	switch(no)
	{
	case 0:
		std::cout << satler_filteredSlave_P.bX << std::endl;
		break;

	case 1:
		std::cout << satler_filteredSlave_P.bY << std::endl;
		break;

		//...
	case 2:
		std::cout << satler_filteredSlave_P.bZ << std::endl;
		break;

	default:
		std::cout << "No such a number!" << std::endl;
		break;
	}

}
/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Slave_side_telemanipulation)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Slave_side_telemanipulation)
