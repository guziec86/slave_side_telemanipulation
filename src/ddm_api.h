/*
 Author: Wojciech Domski
 Version: 1.1.0

 DDM API for decoding the DDM status and exam type
 */

#ifndef DDM_API_HEADER
#define DDM_API_HEADER

#define DDM_CODE_MASK		0x3f
#define DDM_CODE_SIZE		6
#define DDM_CODE_SHIFT	0

#define DDM_SET_STATUS_CODE(code, status)	\
code = ((code & (~ (DDM_CODE_MASK << DDM_CODE_SHIFT)))	\
| ((status & DDM_CODE_MASK) << DDM_CODE_SHIFT))

#define DDM_GET_STATUS_CODE(code)	\
((code & (DDM_CODE_MASK << DDM_CODE_SHIFT)) >> DDM_CODE_SHIFT)

#define DDM_EXAM_MASK		0x07
#define DDM_EXAM_SIZE		3
#define DDM_EXAM_SHIFT	DDM_CODE_SIZE

#define DDM_SET_STATUS_EXAM(code, status)	\
code = ((code & (~ (DDM_EXAM_MASK << DDM_EXAM_SHIFT)))	\
| ((status & DDM_EXAM_MASK) << DDM_EXAM_SHIFT))

#define DDM_GET_STATUS_EXAM(code)	\
((code & (DDM_EXAM_MASK << DDM_EXAM_SHIFT)) >> DDM_EXAM_SHIFT)

#define DDM_PEDAL_MASK		0x01
#define DDM_PEDAL_SIZE		1
#define DDM_PEDAL_SHIFT		(DDM_CODE_SIZE + DDM_EXAM_SIZE)

#define DDM_SET_STATUS_PEDAL(code, status)	\
code = ((code & (~ (DDM_PEDAL_MASK << DDM_PEDAL_SHIFT)))	\
| ((status & DDM_PEDAL_MASK) << DDM_PEDAL_SHIFT))

#define DDM_GET_STATUS_PEDAL(code)	\
((code & (DDM_PEDAL_MASK << DDM_PEDAL_SHIFT)) >> DDM_PEDAL_SHIFT)

/*
 initialization of the DDM
 */
#define DDM_CODE_INIT							0
/*
 DDM is in shutdown mode, power have been cut off or
 the system is shutting down properly
 */
#define DDM_CODE_SHUTDOWN						1
/*
 failure of DDM caused by failure of the other components
 DDM can exit this state and function properly
 this failure happened before entering into examination mode
 */
#define DDM_CODE_FAILURE						2
/*
 DDM is running all the basic components have been initialized
 */
#define DDM_CODE_STANDBY    					3
#define DDM_CODE_SYSTEM_RECONFIGURATION			3
/*
 DDM is ready for operation and awaits further instructions
 like selecting a examination type
 */
#define DDM_CODE_READY      					4
/*
 DDM is synchronizing e.g. waiting for all sides to come
 online and operating
 */
#define DDM_CODE_SYNCHRO						5
/*
 DDM is in examination mode, doctor is performing examination
 */
#define DDM_CODE_EXAM							6
/*
 DDM was requested to change the examination type; one of three
 */
#define DDM_CODE_CHANGE_CONFG					7
/*
 something went wrong during e.g. examination
 */
#define DDM_CODE_EXIT_FAILURE					8

/*
 preparing examination,
 proper exam wasn't yet selected
 */
#define DDM_EXAM_PREPARATION					0
/*
 palpation
 */
#define DDM_EXAM_PALPATION						2
/*
 ultrasonography
 */
#define DDM_EXAM_USG							3
/*
 auscultation
 */
#define DDM_EXAM_AUSCULTATION					1
/*
 review
 */
#define DDM_EXAM_REVIEW							4

/*
 * status for pedal
 * pedal can be pressed or released
 */
#define DDM_PEDAL_PRESSED						1
#define DDM_PEDAL_RELEASED						0

#endif

