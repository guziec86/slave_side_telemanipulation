/*
 * satler_filtered.h
 *
 * Code generation for model "satler_filtered".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C++ source code generated on : Fri Jan  8 13:38:03 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_satler_filtered_h_
#define RTW_HEADER_satler_filtered_h_
#include <math.h>
#include <string.h>
#ifndef satler_filtered_COMMON_INCLUDES_
# define satler_filtered_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* satler_filtered_COMMON_INCLUDES_ */

#include "satler_filtered_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->ModelData.intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->ModelData.intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->ModelData.odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->ModelData.odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ((rtm)->ModelData.odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ((rtm)->ModelData.odeY = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

/* Block signals for system '<Root>/Wave coding Master Side' */
typedef struct {
  real_T Wu;                           /* '<Root>/Wave coding Master Side' */
  real_T Fm;                           /* '<Root>/Wave coding Master Side' */
} B_WavecodingMasterSide_satler_T;

/* Block signals (auto storage) */
typedef struct {
  B_WavecodingMasterSide_satler_T sf_WavecodingMasterSide2;/* '<Root>/Wave coding Master Side2' */
  B_WavecodingMasterSide_satler_T sf_WavecodingMasterSide1;/* '<Root>/Wave coding Master Side1' */
  B_WavecodingMasterSide_satler_T sf_WavecodingMasterSide;/* '<Root>/Wave coding Master Side' */
} B_satler_filtered_T;

/* Continuous states (auto storage) */
typedef struct {
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn2_CSTATE;          /* '<Root>/Transfer Fcn2' */
  real_T TransferFcn3_CSTATE;          /* '<Root>/Transfer Fcn3' */
} X_satler_filtered_T;

/* State derivatives (auto storage) */
typedef struct {
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn2_CSTATE;          /* '<Root>/Transfer Fcn2' */
  real_T TransferFcn3_CSTATE;          /* '<Root>/Transfer Fcn3' */
} XDot_satler_filtered_T;

/* State disabled  */
typedef struct {
  boolean_T TransferFcn1_CSTATE;       /* '<Root>/Transfer Fcn1' */
  boolean_T TransferFcn2_CSTATE;       /* '<Root>/Transfer Fcn2' */
  boolean_T TransferFcn3_CSTATE;       /* '<Root>/Transfer Fcn3' */
} XDis_satler_filtered_T;

#ifndef ODE3_INTG
#define ODE3_INTG

/* ODE3 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[3];                        /* derivatives */
} ODE3_IntgData;

#endif

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T Vm_cmdX;                      /* '<Root>/Vm_cmdX' */
  real_T Wv_rcvX;                      /* '<Root>/Wv_rcvX' */
  real_T Vm_cmdY;                      /* '<Root>/Vm_cmdY' */
  real_T Wv_rcvY;                      /* '<Root>/Wv_rcvY' */
  real_T Vm_cmdZ;                      /* '<Root>/Vm_cmdZ' */
  real_T Wv_rcvZ;                      /* '<Root>/Wv_rcvZ' */
} ExtU_satler_filtered_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Wu_cmdX;                      /* '<Root>/Wu_cmdX' */
  real_T Fs_rcvX;                      /* '<Root>/Fs_rcvX' */
  real_T Wu_cmdY;                      /* '<Root>/Wu_cmdY' */
  real_T Fs_rcvY;                      /* '<Root>/Fs_rcvY' */
  real_T Wu_cmdZ;                      /* '<Root>/Wu_cmdZ' */
  real_T Fs_rcvZ;                      /* '<Root>/Fs_rcvZ' */
} ExtY_satler_filtered_T;

/* Parameters (auto storage) */
struct P_satler_filtered_T_ {
  real_T bX;                           /* Variable: bX
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T bY;                           /* Variable: bY
                                        * Referenced by: '<Root>/Constant1'
                                        */
  real_T bZ;                           /* Variable: bZ
                                        * Referenced by: '<Root>/Constant2'
                                        */
  real_T TransferFcn1_A;               /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn1_C;               /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn2_A;               /* Computed Parameter: TransferFcn2_A
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
  real_T TransferFcn2_C;               /* Computed Parameter: TransferFcn2_C
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
  real_T TransferFcn3_A;               /* Computed Parameter: TransferFcn3_A
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
  real_T TransferFcn3_C;               /* Computed Parameter: TransferFcn3_C
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_satler_filtered_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    X_satler_filtered_T *contStates;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeY[3];
    real_T odeF[3][3];
    ODE3_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

#ifdef __cplusplus

extern "C" {

#endif

#ifdef __cplusplus

}
#endif

/* Class declaration for model satler_filtered */
class satler_filteredModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_satler_filtered_T satler_filtered_U;

  /* External outputs */
  ExtY_satler_filtered_T satler_filtered_Y;

  /* Tunable parameters */
    P_satler_filtered_T satler_filtered_P;

  /* Model entry point functions */

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  satler_filteredModelClass();

  /* Destructor */
  ~satler_filteredModelClass();

  /* Real-Time Model get method */
  RT_MODEL_satler_filtered_T * getRTM();

  /* private data and function members */
 private:


  /* Block signals */
  B_satler_filtered_T satler_filtered_B;
  X_satler_filtered_T satler_filtered_X;/* Block continuous states */

  /* Real-Time Model */
  RT_MODEL_satler_filtered_T satler_filtered_M;

  /* Continuous states update member function*/
  void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si );

  /* Derivatives member function */
  void satler_filtered_derivatives();
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'satler_filtered'
 * '<S1>'   : 'satler_filtered/Wave coding Master Side'
 * '<S2>'   : 'satler_filtered/Wave coding Master Side1'
 * '<S3>'   : 'satler_filtered/Wave coding Master Side2'
 */
#endif                                 /* RTW_HEADER_satler_filtered_h_ */
