/*
 * satler_filteredSlave.h
 *
 * Code generation for model "satler_filteredSlave".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C++ source code generated on : Mon Jan 16 18:34:22 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_satler_filteredSlave_h_
#define RTW_HEADER_satler_filteredSlave_h_
#include <math.h>
#include <string.h>
#ifndef satler_filteredSlave_COMMON_INCLUDES_
# define satler_filteredSlave_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* satler_filteredSlave_COMMON_INCLUDES_ */

#include "satler_filteredSlave_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->ModelData.intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->ModelData.intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->ModelData.odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->ModelData.odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ((rtm)->ModelData.odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ((rtm)->ModelData.odeY = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

/* Block signals for system '<Root>/Wave coding Slave Side' */
typedef struct {
  real_T Wv;                           /* '<Root>/Wave coding Slave Side' */
  real_T Vs_des;                       /* '<Root>/Wave coding Slave Side' */
} B_WavecodingSlaveSide_satler__T;

/* Block signals (auto storage) */
typedef struct {
  B_WavecodingSlaveSide_satler__T sf_WavecodingSlaveSide2;/* '<Root>/Wave coding Slave Side2' */
  B_WavecodingSlaveSide_satler__T sf_WavecodingSlaveSide1;/* '<Root>/Wave coding Slave Side1' */
  B_WavecodingSlaveSide_satler__T sf_WavecodingSlaveSide;/* '<Root>/Wave coding Slave Side' */
} B_satler_filteredSlave_T;

/* Continuous states (auto storage) */
typedef struct {
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn2_CSTATE;          /* '<Root>/Transfer Fcn2' */
  real_T TransferFcn3_CSTATE;          /* '<Root>/Transfer Fcn3' */
} X_satler_filteredSlave_T;

/* State derivatives (auto storage) */
typedef struct {
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn2_CSTATE;          /* '<Root>/Transfer Fcn2' */
  real_T TransferFcn3_CSTATE;          /* '<Root>/Transfer Fcn3' */
} XDot_satler_filteredSlave_T;

/* State disabled  */
typedef struct {
  boolean_T TransferFcn1_CSTATE;       /* '<Root>/Transfer Fcn1' */
  boolean_T TransferFcn2_CSTATE;       /* '<Root>/Transfer Fcn2' */
  boolean_T TransferFcn3_CSTATE;       /* '<Root>/Transfer Fcn3' */
} XDis_satler_filteredSlave_T;

#ifndef ODE3_INTG
#define ODE3_INTG

/* ODE3 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[3];                        /* derivatives */
} ODE3_IntgData;

#endif

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T Fs_cmdX;                      /* '<Root>/Fs_cmdX' */
  real_T Wu_rcvX;                      /* '<Root>/Wu_rcvX' */
  real_T Fs_cmdY;                      /* '<Root>/Fs_cmdY' */
  real_T Wu_rcvY;                      /* '<Root>/Wu_rcvY' */
  real_T Fs_cmdZ;                      /* '<Root>/Fs_cmdZ' */
  real_T Wu_rcvZ;                      /* '<Root>/Wu_rcvZ' */
} ExtU_satler_filteredSlave_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Wv_cmdX;                      /* '<Root>/Wv_cmdX' */
  real_T Vs_rcvX;                      /* '<Root>/Vs_rcvX' */
  real_T Wv_cmdY;                      /* '<Root>/Wv_cmdY' */
  real_T Vs_rcvY;                      /* '<Root>/Vs_rcvY' */
  real_T Wv_cmdZ;                      /* '<Root>/Wv_cmdZ' */
  real_T Vs_rcvZ;                      /* '<Root>/Vs_rcvZ' */
} ExtY_satler_filteredSlave_T;

/* Parameters (auto storage) */
struct P_satler_filteredSlave_T_ {
  real_T bX;                           /* Variable: bX
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T bY;                           /* Variable: bY
                                        * Referenced by: '<Root>/Constant1'
                                        */
  real_T bZ;                           /* Variable: bZ
                                        * Referenced by: '<Root>/Constant2'
                                        */
  real_T TransferFcn1_A;               /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn1_C;               /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn2_A;               /* Computed Parameter: TransferFcn2_A
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
  real_T TransferFcn2_C;               /* Computed Parameter: TransferFcn2_C
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
  real_T TransferFcn3_A;               /* Computed Parameter: TransferFcn3_A
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
  real_T TransferFcn3_C;               /* Computed Parameter: TransferFcn3_C
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_satler_filteredSlave_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    X_satler_filteredSlave_T *contStates;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeY[3];
    real_T odeF[3][3];
    ODE3_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

#ifdef __cplusplus

extern "C" {

#endif

#ifdef __cplusplus

}
#endif

/* Class declaration for model satler_filteredSlave */
class satler_filteredSlaveModelClass {
  /* public data and function members */
 public:
  /* External inputs */
  ExtU_satler_filteredSlave_T satler_filteredSlave_U;

  /* External outputs */
  ExtY_satler_filteredSlave_T satler_filteredSlave_Y;

  /* Model entry point functions */

  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  satler_filteredSlaveModelClass();

  /* Destructor */
  ~satler_filteredSlaveModelClass();

  /* Real-Time Model get method */
  RT_MODEL_satler_filteredSlave_T * getRTM();

/* Tunable parameters */
  P_satler_filteredSlave_T satler_filteredSlave_P;

  /* private data and function members */
 private:
  

  /* Block signals */
  B_satler_filteredSlave_T satler_filteredSlave_B;
  X_satler_filteredSlave_T satler_filteredSlave_X;/* Block continuous states */

  /* Real-Time Model */
  RT_MODEL_satler_filteredSlave_T satler_filteredSlave_M;

  /* Continuous states update member function*/
  void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si );

  /* Derivatives member function */
  void satler_filteredSlave_derivatives();
};

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'satler_filteredSlave'
 * '<S1>'   : 'satler_filteredSlave/Wave coding Slave Side'
 * '<S2>'   : 'satler_filteredSlave/Wave coding Slave Side1'
 * '<S3>'   : 'satler_filteredSlave/Wave coding Slave Side2'
 */
#endif                                 /* RTW_HEADER_satler_filteredSlave_h_ */
