/*
 * satler_filteredSlave_types.h
 *
 * Code generation for model "satler_filteredSlave".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C++ source code generated on : Mon Jan 16 18:34:22 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_satler_filteredSlave_types_h_
#define RTW_HEADER_satler_filteredSlave_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_satler_filteredSlave_T_ P_satler_filteredSlave_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_satler_filteredSlave_T RT_MODEL_satler_filteredSlave_T;

#endif                                 /* RTW_HEADER_satler_filteredSlave_types_h_ */
