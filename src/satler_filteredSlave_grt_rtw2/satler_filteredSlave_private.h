/*
 * satler_filteredSlave_private.h
 *
 * Code generation for model "satler_filteredSlave".
 *
 * Model version              : 1.13
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C++ source code generated on : Tue Dec 27 21:07:10 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_satler_filteredSlave_private_h_
#define RTW_HEADER_satler_filteredSlave_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#endif                                 /* __RTWTYPES_H__ */

extern void satler_filt_WavecodingSlaveSide(real_T rtu_Fs, real_T rtu_Wu, real_T
  rtu_b, B_WavecodingSlaveSide_satler__T *localB);

/* private model entry point functions */
extern void satler_filteredSlave_derivatives();

#endif                                 /* RTW_HEADER_satler_filteredSlave_private_h_ */
