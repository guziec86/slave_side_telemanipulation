/*
 * satler_filteredSlave.cpp
 *
 * Code generation for model "satler_filteredSlave".
 *
 * Model version              : 1.13
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C++ source code generated on : Tue Dec 27 21:07:10 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "satler_filteredSlave.h"
#include "satler_filteredSlave_private.h"

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
void satler_filteredSlaveModelClass::rt_ertODEUpdateContinuousStates
  (RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 3;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  satler_filteredSlave_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  this->step();
  satler_filteredSlave_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  this->step();
  satler_filteredSlave_derivatives();

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/*
 * Output and update for atomic system:
 *    '<Root>/Wave coding Slave Side'
 *    '<Root>/Wave coding Slave Side1'
 *    '<Root>/Wave coding Slave Side2'
 */
void satler_filt_WavecodingSlaveSide(real_T rtu_Fs, real_T rtu_Wu, real_T rtu_b,
  B_WavecodingSlaveSide_satler__T *localB)
{
  /* MATLAB Function 'Wave coding Slave Side': '<S1>:1' */
  /* '<S1>:1:4' */
  //aa
  localB->Vs_des = (sqrt(2.0 * rtu_b) * rtu_Wu - rtu_Fs) / rtu_b;

  /* '<S1>:1:5' */
  localB->Wv = rtu_Wu - sqrt(2.0 / rtu_b) * rtu_Fs;

  /*  SATLER */
  /*     Wv = sqrt(2/b)*Fs - Wu;    %PAPER SPONG */
}

/* Model step function */
void satler_filteredSlaveModelClass::step()
{
  if (rtmIsMajorTimeStep((&satler_filteredSlave_M))) {
    /* set solver stop time */
    if (!((&satler_filteredSlave_M)->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&(&satler_filteredSlave_M)->solverInfo,
                            (((&satler_filteredSlave_M)->Timing.clockTickH0 + 1)
        * (&satler_filteredSlave_M)->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&(&satler_filteredSlave_M)->solverInfo,
                            (((&satler_filteredSlave_M)->Timing.clockTick0 + 1) *
                             (&satler_filteredSlave_M)->Timing.stepSize0 +
        (&satler_filteredSlave_M)->Timing.clockTickH0 * (&satler_filteredSlave_M)
        ->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep((&satler_filteredSlave_M))) {
    (&satler_filteredSlave_M)->Timing.t[0] = rtsiGetT(&(&satler_filteredSlave_M
      )->solverInfo);
  }

  /* Outport: '<Root>/Wv_cmdX' incorporates:
   *  TransferFcn: '<Root>/Transfer Fcn1'
   */
  satler_filteredSlave_Y.Wv_cmdX = satler_filteredSlave_P.TransferFcn1_C *
    satler_filteredSlave_X.TransferFcn1_CSTATE;

  /* MATLAB Function: '<Root>/Wave coding Slave Side' incorporates:
   *  Constant: '<Root>/Constant'
   *  Inport: '<Root>/Fs_cmdX'
   *  Inport: '<Root>/Wu_rcvX'
   */
  satler_filt_WavecodingSlaveSide(satler_filteredSlave_U.Fs_cmdX,
    satler_filteredSlave_U.Wu_rcvX, satler_filteredSlave_P.bX,
    &satler_filteredSlave_B.sf_WavecodingSlaveSide);

  /* Outport: '<Root>/Vs_rcvX' */
  satler_filteredSlave_Y.Vs_rcvX =
    satler_filteredSlave_B.sf_WavecodingSlaveSide.Vs_des;

  /* Outport: '<Root>/Wv_cmdY' incorporates:
   *  TransferFcn: '<Root>/Transfer Fcn2'
   */
  satler_filteredSlave_Y.Wv_cmdY = satler_filteredSlave_P.TransferFcn2_C *
    satler_filteredSlave_X.TransferFcn2_CSTATE;

  /* MATLAB Function: '<Root>/Wave coding Slave Side1' incorporates:
   *  Constant: '<Root>/Constant1'
   *  Inport: '<Root>/Fs_cmdY'
   *  Inport: '<Root>/Wu_rcvY'
   */
  satler_filt_WavecodingSlaveSide(satler_filteredSlave_U.Fs_cmdY,
    satler_filteredSlave_U.Wu_rcvY, satler_filteredSlave_P.bY,
    &satler_filteredSlave_B.sf_WavecodingSlaveSide1);

  /* Outport: '<Root>/Vs_rcvY' */
  satler_filteredSlave_Y.Vs_rcvY =
    satler_filteredSlave_B.sf_WavecodingSlaveSide1.Vs_des;

  /* Outport: '<Root>/Wv_cmdZ' incorporates:
   *  TransferFcn: '<Root>/Transfer Fcn3'
   */
  satler_filteredSlave_Y.Wv_cmdZ = satler_filteredSlave_P.TransferFcn3_C *
    satler_filteredSlave_X.TransferFcn3_CSTATE;

  /* MATLAB Function: '<Root>/Wave coding Slave Side2' incorporates:
   *  Constant: '<Root>/Constant2'
   *  Inport: '<Root>/Fs_cmdZ'
   *  Inport: '<Root>/Wu_rcvZ'
   */
  satler_filt_WavecodingSlaveSide(satler_filteredSlave_U.Fs_cmdZ,
    satler_filteredSlave_U.Wu_rcvZ, satler_filteredSlave_P.bZ,
    &satler_filteredSlave_B.sf_WavecodingSlaveSide2);

  /* Outport: '<Root>/Vs_rcvZ' */
  satler_filteredSlave_Y.Vs_rcvZ =
    satler_filteredSlave_B.sf_WavecodingSlaveSide2.Vs_des;
  if (rtmIsMajorTimeStep((&satler_filteredSlave_M))) {
    rt_ertODEUpdateContinuousStates(&(&satler_filteredSlave_M)->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++(&satler_filteredSlave_M)->Timing.clockTick0)) {
      ++(&satler_filteredSlave_M)->Timing.clockTickH0;
    }

    (&satler_filteredSlave_M)->Timing.t[0] = rtsiGetSolverStopTime
      (&(&satler_filteredSlave_M)->solverInfo);

    {
      /* Update absolute timer for sample time: [0.001s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.001, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      (&satler_filteredSlave_M)->Timing.clockTick1++;
      if (!(&satler_filteredSlave_M)->Timing.clockTick1) {
        (&satler_filteredSlave_M)->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void satler_filteredSlaveModelClass::satler_filteredSlave_derivatives()
{
  XDot_satler_filteredSlave_T *_rtXdot;
  _rtXdot = ((XDot_satler_filteredSlave_T *) (&satler_filteredSlave_M)
             ->ModelData.derivs);

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn1' */
  _rtXdot->TransferFcn1_CSTATE = 0.0;
  _rtXdot->TransferFcn1_CSTATE += satler_filteredSlave_P.TransferFcn1_A *
    satler_filteredSlave_X.TransferFcn1_CSTATE;
  _rtXdot->TransferFcn1_CSTATE +=
    satler_filteredSlave_B.sf_WavecodingSlaveSide.Wv;

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn2' */
  _rtXdot->TransferFcn2_CSTATE = 0.0;
  _rtXdot->TransferFcn2_CSTATE += satler_filteredSlave_P.TransferFcn2_A *
    satler_filteredSlave_X.TransferFcn2_CSTATE;
  _rtXdot->TransferFcn2_CSTATE +=
    satler_filteredSlave_B.sf_WavecodingSlaveSide1.Wv;

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn3' */
  _rtXdot->TransferFcn3_CSTATE = 0.0;
  _rtXdot->TransferFcn3_CSTATE += satler_filteredSlave_P.TransferFcn3_A *
    satler_filteredSlave_X.TransferFcn3_CSTATE;
  _rtXdot->TransferFcn3_CSTATE +=
    satler_filteredSlave_B.sf_WavecodingSlaveSide2.Wv;
}

/* Model initialize function */
void satler_filteredSlaveModelClass::initialize()
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)(&satler_filteredSlave_M), 0,
                sizeof(RT_MODEL_satler_filteredSlave_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&(&satler_filteredSlave_M)->solverInfo,
                          &(&satler_filteredSlave_M)->Timing.simTimeStep);
    rtsiSetTPtr(&(&satler_filteredSlave_M)->solverInfo, &rtmGetTPtr
                ((&satler_filteredSlave_M)));
    rtsiSetStepSizePtr(&(&satler_filteredSlave_M)->solverInfo,
                       &(&satler_filteredSlave_M)->Timing.stepSize0);
    rtsiSetdXPtr(&(&satler_filteredSlave_M)->solverInfo,
                 &(&satler_filteredSlave_M)->ModelData.derivs);
    rtsiSetContStatesPtr(&(&satler_filteredSlave_M)->solverInfo, (real_T **)
                         &(&satler_filteredSlave_M)->ModelData.contStates);
    rtsiSetNumContStatesPtr(&(&satler_filteredSlave_M)->solverInfo,
      &(&satler_filteredSlave_M)->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&(&satler_filteredSlave_M)->solverInfo,
                          (&rtmGetErrorStatus((&satler_filteredSlave_M))));
    rtsiSetRTModelPtr(&(&satler_filteredSlave_M)->solverInfo,
                      (&satler_filteredSlave_M));
  }

  rtsiSetSimTimeStep(&(&satler_filteredSlave_M)->solverInfo, MAJOR_TIME_STEP);
  (&satler_filteredSlave_M)->ModelData.intgData.y = (&satler_filteredSlave_M)
    ->ModelData.odeY;
  (&satler_filteredSlave_M)->ModelData.intgData.f[0] = (&satler_filteredSlave_M
    )->ModelData.odeF[0];
  (&satler_filteredSlave_M)->ModelData.intgData.f[1] = (&satler_filteredSlave_M
    )->ModelData.odeF[1];
  (&satler_filteredSlave_M)->ModelData.intgData.f[2] = (&satler_filteredSlave_M
    )->ModelData.odeF[2];
  (&satler_filteredSlave_M)->ModelData.contStates = ((X_satler_filteredSlave_T *)
    &satler_filteredSlave_X);
  rtsiSetSolverData(&(&satler_filteredSlave_M)->solverInfo, (void *)
                    &(&satler_filteredSlave_M)->ModelData.intgData);
  rtsiSetSolverName(&(&satler_filteredSlave_M)->solverInfo,"ode3");
  rtmSetTPtr((&satler_filteredSlave_M), &(&satler_filteredSlave_M)
             ->Timing.tArray[0]);
  (&satler_filteredSlave_M)->Timing.stepSize0 = 0.001;

  /* block I/O */
  (void) memset(((void *) &satler_filteredSlave_B), 0,
                sizeof(B_satler_filteredSlave_T));

  /* states (continuous) */
  {
    (void) memset((void *)&satler_filteredSlave_X, 0,
                  sizeof(X_satler_filteredSlave_T));
  }

  /* external inputs */
  (void) memset((void *)&satler_filteredSlave_U, 0,
                sizeof(ExtU_satler_filteredSlave_T));

  /* external outputs */
  (void) memset((void *)&satler_filteredSlave_Y, 0,
                sizeof(ExtY_satler_filteredSlave_T));

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn1' */
  satler_filteredSlave_X.TransferFcn1_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn2' */
  satler_filteredSlave_X.TransferFcn2_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn3' */
  satler_filteredSlave_X.TransferFcn3_CSTATE = 0.0;
}

/* Model terminate function */
void satler_filteredSlaveModelClass::terminate()
{
  /* (no terminate code required) */
}

/* Constructor */
satler_filteredSlaveModelClass::satler_filteredSlaveModelClass()
{
  P_satler_filteredSlave_T satler_filteredSlave_P_temp = {
    10.0,                              /* Variable: bX
                                        * Referenced by: '<Root>/Constant'
                                        */
    10.0,                              /* Variable: bY
                                        * Referenced by: '<Root>/Constant1'
                                        */
    10.0,                              /* Variable: bZ
                                        * Referenced by: '<Root>/Constant2'
                                        */
    -62.831853071795862,               /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
    62.831853071795862,                /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
    -62.831853071795862,               /* Computed Parameter: TransferFcn2_A
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
    62.831853071795862,                /* Computed Parameter: TransferFcn2_C
                                        * Referenced by: '<Root>/Transfer Fcn2'
                                        */
    -62.831853071795862,               /* Computed Parameter: TransferFcn3_A
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
    62.831853071795862                 /* Computed Parameter: TransferFcn3_C
                                        * Referenced by: '<Root>/Transfer Fcn3'
                                        */
  };                                   /* Modifiable parameters */

  /* Initialize tunable parameters */
  satler_filteredSlave_P = satler_filteredSlave_P_temp;
}

/* Destructor */
satler_filteredSlaveModelClass::~satler_filteredSlaveModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_satler_filteredSlave_T * satler_filteredSlaveModelClass::getRTM()
{
  return (&satler_filteredSlave_M);
}
