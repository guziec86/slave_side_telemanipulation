#ifndef OROCOS_Slave_side_telemanipulation_COMPONENT_HPP
#define OROCOS_Slave_side_telemanipulation_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt_rosclock/rtt_rosclock.h>
#include "satler_filteredSlave_grt_rtw/satler_filteredSlave.h"
#include "ddm_api.h"

#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>

#include <WRUT/TelemanipulationData.h>
#include <remedi_usg_ros_udp/Teleop_Feedback.h>
#include <remedi_usg_ros_udp/ArmFeedback.h>
//#include <std_msgs/Float64MultiArray.h>


#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/TwistStamped.h>

class Slave_side_telemanipulation :
		public RTT::TaskContext,
		public satler_filteredSlaveModelClass{

public:

	//	pose_pub_     = ros_node_->advertise<geometry_msgs::PoseStamped>("endeffector_position", 10);
	//		velocity_pub_ = ros_node_->advertise<geometry_msgs::TwistStamped>("endeffector_velocity", 10);
	//		force_pub_    = ros_node_->advertise<geometry_msgs::WrenchStamped>("endeffector_force", 10);
	//		status_pub_   = ros_node_->advertise<percro_6dofhaptic::Status>("endeffector_status", 10);

	RTT::InputPort<WRUT::TelemanipulationData> InputWavePortToRobot; //data containig 3 wave variables
	RTT::InputPort<remedi_usg_ros_udp::ArmFeedback> FromRobotForcePort; //data containing 3 forces

	RTT::OutputPort<remedi_usg_ros_udp::Teleop_Feedback> OutputWavePortFromRobot; 			//data containing 3 wave variables
	RTT::OutputPort<geometry_msgs::TwistStamped> RobotCmdVelocityPort; // velocity form delta

	RTT::OutputPort<std_msgs::UInt32> Telemanipulation_Status_out;  //output to DDM

	RTT::OutputPort<geometry_msgs::TwistStamped> DecodedVelocityPort; // velocity form delta
//RTT::OutputPort<geometry_msgs::WrenchStamped> DecodedForcePort; //data containing 3 forces



	//RTT::OutputPort<std::vector<double> > Control;

	// inputs for wave coding module
	double Wu_rcv; //control from master TODO FILTERING
	double Fs_cms; //received force from the sensor

	// outputs of a wave coding module
	double Vs_cmd; //master`s demanded velocity
	double Wv_cmd; //received slave demanded velocity

	int actual_state;

	//wave impedancy is in matlab  model

	Slave_side_telemanipulation(std::string const& name);
	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();

	void printOutData();
	void assignmentOfInputsWave(WRUT::TelemanipulationData, remedi_usg_ros_udp::ArmFeedback);
	void setOutputValuesAndSend(geometry_msgs::PoseStamped, remedi_usg_ros_udp::ArmFeedback);

	void setb(float , int );
	void getb(int);

	void waveCoding(void );

	void setInputWaveDataAvailable(void ){
		InputWaveDataAvailable = true;
	}

	void setRobotForceDataAvailable(void ){
		RobotForceDataAvailable = true;
	}

	void clearInputWaveDataAvailable(void ){
		InputWaveDataAvailable = false;
	}

	void clearRobotForceDataAvailable(void ){
		RobotForceDataAvailable = false;
	}

	bool isInputWaveDataAvailable(void ){
		return InputWaveDataAvailable;
	}

	bool isRobotForceDataAvailable(void ){
		return RobotForceDataAvailable;
	}

private:
	double b;
	bool InputWaveDataAvailable;
	bool RobotForceDataAvailable;
};
#endif
