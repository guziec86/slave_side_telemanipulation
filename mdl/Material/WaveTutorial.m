% addpath('.')
disp('========================== STEP 1 ==================================')
disp('The first step is to define the environment model.')
disp('Press Return to open an example, a second return to close')
pause
open_system('Step1BuildEnvironment')
pause
close_system('Step1BuildEnvironment')

disp('========================== STEP 2 ==================================')
disp('In a second step we add an elementary motor & slave system model')
disp('We only suppose to add the device mass m, and suppose that motors')
disp('are linear, no viscosity and unit gain (between controller command')
disp('and the exerted force')
disp('Press Return to open an example, a second return to close')
pause
open_system('Step2SlaveMotorConnection')
pause
mysys = (linmod('Step2SlaveMotorConnection'));
close_system('Step2SlaveMotorConnection')
[n,d]=ss2tf(mysys.a,mysys.b,mysys.c,mysys.d);
disp('Transfer function (around a linearization point) can be left to');
disp(' simple simulink analysis');
tf(n,d)

disp('========================== STEP 3 ==================================')
disp('In a third step we add a local controller to the SLAVE side in order')
disp('to stabilize the system (if required) and to provide required performance.')
disp('This is the controller that assures the stability of the SLAVE.')
disp('Press Return to open an example, a second return to close')
pause
open_system('Step3AddSensorAndSlaveController')
pause
close_system('Step3AddSensorAndSlaveController')

disp('========================== STEP 4 ==================================')
disp('In a fourth step we add a MASTER side in a way which is similar to the')
disp('SLAVE one with the exeption that this is a impedance interface.')
disp('Assuming a high quality controller, the force commanded is')
disp('directly sent to the device interface');
disp('Press Return to open an example, a second return to close')
pause
open_system('Step4AddMasterTeleOp');
pause
close_system('Step4AddMasterTeleOp');

disp('========================== STEP 5 ==================================')
disp('We add a transmission delay both in the forward and backward direction');
disp('The system get instability');
pause
open_system('Step5AddTransmissionDelay');
% fDelay = 0.15;
% bDelay = 0.15;
set_param([bdroot '/tdf'],'DelayTime','0.15');
set_param([bdroot '/tdb'],'DelayTime','0.15');
pause
close_system('Step5AddTransmissionDelay')


disp('========================== STEP 6 ==================================')
disp('The wave variables are introduced in order to cope with the comunication');
disp('delay.');
disp('The wave impedance has been tuned on the system and the impedance reflection');
disp('has been reduced by means of wave filters');
disp('On the real system an ad-hoc impedance matching can be implemented');
pause
open_system('Step6AddWaveVariables');
% fDelay = 0.15;
% bDelay = 0.15;
set_param([bdroot '/tdf'],'DelayTime','0.15');
set_param([bdroot '/tdb'],'DelayTime','0.15');
pause
close_system('Step6AddWaveVariables');

